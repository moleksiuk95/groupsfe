import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../shared/services/admin.service';
import {GroupModel} from '../../Models/GroupModel';
import {UsersInGroupsModel} from '../../Models/UsersInGroupsModel';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.scss']
})
export class GroupsListComponent implements OnInit {

  constructor(private adminService: AdminService) { }

  public groupsList = Array<GroupModel>();
  public usersInGroupsModel: UsersInGroupsModel;
  public newGroup = {
    groupName: '',
    groupDescr: '',
    id: 0,
    userCount: 0
  };
  public tempGroup: GroupModel;
  public groupWasAdded = false;

  public editGroups = {};
  public editUsersInGroups = {};
  public tempUsersInGroup = {};
  public selectedUsersByGroups = {};

  ngOnInit() {
    this.adminService.GetAllGroups().subscribe(
      response => {
        this.groupsList = response;
        console.log(response);
      }, err => {
        console.error(err);
      }
    );
  }

  public addNewGroup() {
    this.groupWasAdded = true;
    if (!this.newGroup.groupName) {
      return;
    }
    this.adminService.CreateGroup(this.newGroup).subscribe(
      response => {
        this.groupsList.unshift(response);
        this.newGroup = {
          groupName: '',
          groupDescr: '',
          id: 0,
          userCount: 0
        };
        this.groupWasAdded = false;
      }, err => {
        console.error(err);
      }
    );
    console.log(this.newGroup);
  }

  public editGroup(group: GroupModel) {
    this.editGroups[group.id] = true;
    this.tempGroup = Object.assign({}, group);
  }

  public removeGroup(group: GroupModel, idx: number) {
    if (confirm('Removed group ' + group.groupName + ' [ID=' + group.id + ']?')) {
      this.adminService.RemoveGroup(group).subscribe(
        response => {
          this.groupsList.splice(idx, 1);
        }, err => {
          console.error(err);
        }
      );
    }
  }
  public editGroupConfirm(group: GroupModel, idx: number) {
    this.adminService.EditGroup(group).subscribe(
      response => {
        this.groupsList[idx] = response;
        this.editGroups[this.tempGroup.id] = false;
      }, err => {
        console.error(err);
      }
    );
  }

  public editGroupCancel(idx: number) {
    this.groupsList[idx] = this.tempGroup;
    this.editGroups[this.tempGroup.id] = false;
  }

  public usersInGroupEdit(group: GroupModel) {
    this.adminService.GetUsersInGroup(group.id).subscribe(
      response => {
        this.usersInGroupsModel = response;
        this.editUsersInGroups[group.id] = true;
        this.selectedUsersByGroups[group.id] = response.usersInGroup;
        this.tempGroup = Object.assign({}, group);
        console.log(response);
      }, err => {
        console.error(err);
      }
    );
  }

  public usersInGroupEditConfirm(group: GroupModel, idx: number) {
    this.adminService.ChangeUserInGroup(group.id, this.selectedUsersByGroups[group.id]).subscribe(
      response => {
        group.userCount = response;
        this.groupsList[idx] = group;
        this.editUsersInGroups[group.id] = false;
      }, err => {
        console.error(err);
      }
    );
  }

  public usersInGroupEditCancel(group: GroupModel, idx: number) {
    this.groupsList[idx] = this.tempGroup;
    this.editUsersInGroups[group.id] = false;
  }

}
