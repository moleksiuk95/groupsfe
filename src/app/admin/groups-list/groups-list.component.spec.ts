import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import { GroupsListComponent } from './groups-list.component';
import {AdminService} from '../../shared/services/admin.service';

describe('GroupsListComponent', () => {
  let component: GroupsListComponent;
  let fixture: ComponentFixture<GroupsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, FormsModule],
      declarations: [ GroupsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AdminService], (adminService: AdminService) => {
    expect(component).toBeTruthy();
  }));
});
