import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../shared/services/admin.service';
import {UpdatingUser} from '../../Models/UpdatingUser';
import {RegisteredUser} from '../../Models/RegistererdUser';

@Component({
  selector: 'app-edit-user-form',
  templateUrl: './edit-user-form.component.html',
  styleUrls: ['./edit-user-form.component.scss']
})
export class EditUserFormComponent implements OnInit {

  constructor(private adminService: AdminService) { }

  public submitForm = false;
  public serverError = false;
  public serverErrorMsg = 'Server error';
  public updatingUser = new UpdatingUser();
  public EditForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ])),
    birthday: new FormControl(''),
    gender: new FormControl('1')
  });

  @Input()
  userModel: RegisteredUser;
  @Output()
  changeUser = new EventEmitter();

  ngOnInit() {
    if (this.userModel) {
      this.updatingUser.oldEmail = this.userModel.email;
      this.initForm();
    }
  }

  public initForm() {
    const gender = this.userModel.gender.toString();
    const minDate = new Date('1900-1-1');
    let date = new Date(this.userModel.birthday);
    if (date < minDate) {
      date = minDate;
    }
    const datePickerObj = {
      year: date.getFullYear(), month: date.getMonth(), day: date.getDate()
    };
    console.log(date, datePickerObj);
    this.EditForm = new FormGroup({
      firstName: new FormControl(this.userModel.firstName, Validators.required),
      lastName: new FormControl(this.userModel.lastName, Validators.required),
      email: new FormControl(this.userModel.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      birthday: new FormControl(datePickerObj),
      gender: new FormControl(gender)
    });
  }

  public changeConfirm() {
    this.submitForm = true;
    if (this.EditForm.invalid) {
      return;
    } else {
      this.EditForm.value.gender = +this.EditForm.value.gender;
      const bDate = this.EditForm.value.birthday;
      const date = bDate.year + '-' + bDate.month + '-' + bDate.day;
      this.EditForm.value.birthday = new Date(date);
      const updatingUserModel = this.EditForm.value;
      updatingUserModel.oldEmail = this.updatingUser.oldEmail;
      console.log(updatingUserModel);
      this.adminService.UpdateUser(updatingUserModel).subscribe(
        response => {
          this.changeUser.emit(response);
        }, err => {
          console.log(err);
          this.serverError = true;
          this.serverErrorMsg = err.error;
        }
      );
    }
  }

}
