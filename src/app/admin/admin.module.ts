import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import {SharedModule} from '../shared/shared.module';
import { EditUserFormComponent } from './edit-user-form/edit-user-form.component';
import { GroupsListComponent } from './groups-list/groups-list.component';



@NgModule({
  declarations: [AdminProfileComponent, EditUserFormComponent, GroupsListComponent],
    imports: [
      NgbModule,
      FormsModule,
      ReactiveFormsModule,
      CommonModule,
      SharedModule
    ]
})
export class AdminModule { }
