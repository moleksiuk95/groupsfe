import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

import { AdminProfileComponent } from './admin-profile.component';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {UsersService} from '../../shared/services/users.service';
import {ChangePasswordComponent} from '../../shared/components/change-password/change-password.component';
import {EditUserFormComponent} from '../edit-user-form/edit-user-form.component';
import {AdminService} from '../../shared/services/admin.service';
import {GroupsListComponent} from '../groups-list/groups-list.component';
import {RegistryFormComponent} from '../../shared/components/registry-form/registry-form.component';

describe('AdminProfileComponent', () => {
  let component: AdminProfileComponent;
  let fixture: ComponentFixture<AdminProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule, NgbDatepickerModule, RouterTestingModule],
      declarations: [ AdminProfileComponent, ChangePasswordComponent, EditUserFormComponent, GroupsListComponent, RegistryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AuthoriseService, UsersService, Router],
    (authoriseService: AuthoriseService, usersService: UsersService, adminService: AdminService, router: Router) => {
    expect(component).toBeTruthy();
  }));
});
