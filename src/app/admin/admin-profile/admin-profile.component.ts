import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RegisteredUser} from '../../Models/RegistererdUser';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {UsersService} from '../../shared/services/users.service';
import {AdminService} from '../../shared/services/admin.service';
import {UsersInfo} from '../../Models/UsersInfo';
import {Role} from '../../Models/Role';
import {JWTParser} from '../../helpers/JWTParser';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  constructor(private authoriseService: AuthoriseService,
              private usersService: UsersService,
              private adminService: AdminService,
              private router: Router) { }

  public adminUser: RegisteredUser;
  public profileEdit = false;
  public userList = Array<UsersInfo>();
  public editUsers = {};
  public editUserRoles = {};
  public roles = Array<string>();
  public showAddUserForm = false;

  ngOnInit() {
    this.adminUser = this.authoriseService.currentUser;
    if (!this.adminUser) {
      this.authoriseService.GetRegisteredUser().subscribe(
        response => {
          this.adminUser = response;
          this.GetUsers();
        }, err => {
          console.error(err);
          return this.router.navigate(['/login']);
        }
      );
    } else {
      this.GetUsers();
    }
    // tslint:disable-next-line:forin
    for (const r in Role) {
      this.roles.push(Role[r]);
    }
  }

  public AddNewUser() {
    this.showAddUserForm = false;
    this.GetUsers();
  }

  public GetUsers() {
    this.adminService.GetUserList(this.adminUser.email).subscribe(
      response => {
        this.userList = response;
        console.log(this.userList);
      }, err => {
        console.error(err);
      }
    );
  }

  public updateUserModel(updatedUser) {
    this.adminUser = updatedUser;
    this.profileEdit = false;
  }

  public startEdit(userEmail: string) {
    this.editUsers[userEmail] = true;
  }

  public endEdit(userEmail: string) {
    this.editUsers[userEmail] = false;
  }

  public updateUserItem(newModel: RegisteredUser, idx: number) {
    const model = this.userList[idx];
    this.editUsers[model.email] = false;
    model.firstName = newModel.firstName;
    model.lastName = newModel.lastName;
    model.email = newModel.email;
    model.birthday = newModel.birthday;
    model.gender = newModel.gender;
  }

  public removeUser(userEmail: string, idx: number) {
    if (confirm('Delete user ' + userEmail + '?')) {
      this.adminService.RemoveUser(userEmail).subscribe(
        response => {
          this.userList.splice(idx, 1);
        }, err => {
          console.error(err);
        }
      );
    }
  }

  public changeRoleStart(userEmail: string) {
    console.log(userEmail);
    this.editUserRoles[userEmail] = true;
  }

  public changeRoleConfirm(user: UsersInfo, idx: number) {
    this.adminService.ChangeUserRoles({userEmail: user.email, userRole: user.role}).subscribe(
      response => {
        this.editUserRoles[user.email] = false;
      }, err => {
        console.error(err);
      }
    );
  }

  public Logout() {
    JWTParser.ClearToken();
    return this.router.navigate(['/login']);
  }
}
