import { Component, OnInit } from '@angular/core';
import {JWTParser} from '../../helpers/JWTParser';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  public IsLogin: boolean;
  public IsUser: boolean;
  public IsAdmin: boolean;
  public userEmail: string;

  ngOnInit() {
    this.SetTokenFlags();
  }

  public SetTokenFlags() {
    this.IsLogin = JWTParser.CheckToken();
    if (this.IsLogin) {
      this.IsUser = JWTParser.IsUser();
      this.IsAdmin = JWTParser.IsAdmin();
      this.userEmail = JWTParser.GetUserEmail();
    }

  }

}
