import {Observable} from 'rxjs';

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegisteredUser} from '../../Models/RegistererdUser';
import {LoginModel} from '../../Models/LoginModel';
import {ChangePasswordModel} from '../../Models/ChangePasswordModel';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) { }

  public UpdateUser(registeredUser: RegisteredUser): Observable<RegisteredUser> {
    return this.http.post<RegisteredUser>('https://localhost:44325/api/v1/user/update', registeredUser);
  }

  public CheckOldPassword(loginModel: LoginModel): Observable<boolean> {
    return this.http.post<boolean>('https://localhost:44325/api/v1/user/password-check', loginModel);
  }

  public ChangePassword(changePassword: ChangePasswordModel): Observable<any> {
    return this.http.post<any>('https://localhost:44325/api/v1/user/change-password', changePassword);
  }
}
