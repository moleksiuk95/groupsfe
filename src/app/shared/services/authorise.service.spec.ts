import {inject, TestBed} from '@angular/core/testing';

import { AuthoriseService } from './authorise.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('AuthoriseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', inject( [HttpClient], (http: HttpClient) => {
    const service: AuthoriseService = TestBed.get(AuthoriseService);
    expect(service).toBeTruthy();
  }));
});
