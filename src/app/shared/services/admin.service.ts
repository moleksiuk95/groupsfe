import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UpdatingUser} from '../../Models/UpdatingUser';
import {UsersInfo} from '../../Models/UsersInfo';
import {ChangeUserRole} from '../../Models/ChangeUserRole';
import {GroupModel} from '../../Models/GroupModel';
import {UsersInGroupsModel} from '../../Models/UsersInGroupsModel';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) {
  }

  public UpdateUser(updatingUser: UpdatingUser): Observable<UpdatingUser> {
    return this.http.post<UpdatingUser>('https://localhost:44325/api/v1/admin/user-update', updatingUser);
  }

  public GetUserList(email: string): Observable<Array<UsersInfo>> {
    return this.http.get<Array<UsersInfo>>('https://localhost:44325/api/v1/admin/user-list', {params: {userEmail: email}});
  }

  public RemoveUser(email: string): Observable<any> {
    return this.http.delete<any>('https://localhost:44325/api/v1/admin/remove-user', {params: {userEmail: email}});
  }

  public ChangeUserRoles(changeRole: ChangeUserRole): Observable<ChangeUserRole> {
    return this.http.post<ChangeUserRole>('https://localhost:44325/api/v1/admin/change-role', changeRole);
  }

  public GetAllGroups(): Observable<Array<GroupModel>> {
    return this.http.get<Array<GroupModel>>('https://localhost:44325/api/v1/admin/get-groups');
  }

  public CreateGroup(group: GroupModel): Observable<GroupModel> {
    return this.http.post<GroupModel>('https://localhost:44325/api/v1/admin/create-group', group);
  }

  public EditGroup(group: GroupModel): Observable<GroupModel> {
    return this.http.post<GroupModel>('https://localhost:44325/api/v1/admin/edit-group', group);
  }

  public GetUsersInGroup(groupId: number): Observable<UsersInGroupsModel> {
    const grIdStr = groupId.toString();
    return this.http.get<UsersInGroupsModel>('https://localhost:44325/api/v1/admin/users-in-group', {
      params: {grId: grIdStr}});
  }

  public RemoveGroup(group: GroupModel): Observable<any> {
    return this.http.post<any>('https://localhost:44325/api/v1/admin/remove-group', group);
  }

  public ChangeUserInGroup(groupId: number, userEmails: Array<string>): Observable<number> {
    return this.http.post<number>('https://localhost:44325/api/v1/admin/change-users-in-group', {
      key: groupId, value: userEmails
    });
  }

}
