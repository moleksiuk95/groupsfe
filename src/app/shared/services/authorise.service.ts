import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../Models/User';
import {LoginModel} from '../../Models/LoginModel';
import {RegisteredUser} from '../../Models/RegistererdUser';
import {map} from 'rxjs/operators';
import {JWTParser} from '../../helpers/JWTParser';

@Injectable({
  providedIn: 'root'
})
export class AuthoriseService {

  constructor(private http: HttpClient) { }
  public currentUser: RegisteredUser;

  private SetRegisteredUser(user: RegisteredUser) {
    this.currentUser = user;
    if (user && user.token) {
      sessionStorage.JWT = user.token.authToken;
    }
  }

  public GetRegisteredUser(): Observable<RegisteredUser> {
    const email = JWTParser.GetUserEmail();
    return this.http.get<RegisteredUser>('https://localhost:44325/api/v1/auth/get-user',
      {params: {userEmail: email}})
      .pipe(map(user => {
        this.SetRegisteredUser(user);
        return user;
      }));
  }

  public Registry(newUser: User, fromAdmin: boolean): Observable<RegisteredUser> {
    return this.http.post<RegisteredUser>('https://localhost:44325/api/v1/auth/registry', newUser)
      .pipe(map(user => {
        if (!fromAdmin) {
          this.SetRegisteredUser(user);
        }
        return user;
      }));
  }

  public Login(loginModel: LoginModel): Observable<RegisteredUser> {
    return this.http.post<RegisteredUser>('https://localhost:44325/api/v1/auth/login', loginModel)
      .pipe(map(user => {
        this.SetRegisteredUser(user);
        return user;
    }));
  }
}
