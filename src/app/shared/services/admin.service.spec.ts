import {inject, TestBed} from '@angular/core/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { AdminService } from './admin.service';

describe('AdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', inject( [HttpClient], (http: HttpClient) => {
    const service: AdminService = TestBed.get(AdminService);
    expect(service).toBeTruthy();
  }));
});
