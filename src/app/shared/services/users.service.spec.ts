import {inject, TestBed} from '@angular/core/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { UsersService } from './users.service';

describe('UsersService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', inject( [HttpClient], (http: HttpClient) => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  }));
});
