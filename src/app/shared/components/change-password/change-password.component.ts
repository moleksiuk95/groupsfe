import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AuthoriseService} from '../../services/authorise.service';
import {UsersService} from '../../services/users.service';
import {JWTParser} from '../../../helpers/JWTParser';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private authoriseService: AuthoriseService, private usersService: UsersService, private router: Router) { }

  public passwordEdit = false;
  public oldPasswordIsConfirmed = false;
  public changePassword = {
    password: '',
    confirmPassword: '',
    oldPassword: '',
    passwordError: false,
    passwordErrorMsg: '',
    oldPasswordError: false,
    oldPasswordErrorMsg: ''
  };

  @Input()
  public userEmail: string;

  ngOnInit() {
  }

  public Logout() {
    JWTParser.ClearToken();
    return this.router.navigate(['/login']);
  }

  public checkPassword() {
    if (!this.changePassword.oldPassword) {
      this.changePassword.oldPasswordErrorMsg = 'Old password is required';
      this.changePassword.oldPasswordError = true;
      return;
    }
    this.usersService.CheckOldPassword({email: this.userEmail, password: this.changePassword.oldPassword})
      .subscribe(
        response => {
          this.oldPasswordIsConfirmed = true;
          this.changePassword.oldPasswordErrorMsg = '';
          this.changePassword.oldPasswordError = false;
        }, err => {
          console.log('Error', err);
          this.oldPasswordIsConfirmed = false;
          if (err.error === 'User not found') {
            return this.Logout();
          } else {
            this.changePassword.oldPasswordErrorMsg = err.error;
            this.changePassword.oldPasswordError = true;
          }
        }
      );
  }

  public changePswrd() {
    if (!this.changePassword.password || !this.changePassword.confirmPassword) {
      this.changePassword.passwordError = true;
      this.changePassword.passwordErrorMsg = 'This both fields are required';
      return;
    }

    if (this.changePassword.password !== this.changePassword.confirmPassword) {
      this.changePassword.passwordError = true;
      this.changePassword.passwordErrorMsg = 'Passwords does not match';
      return;
    }
    const changePasswordModel = {
      userEmail: this.userEmail,
      oldPassword: this.changePassword.oldPassword,
      newPassword: this.changePassword.password
    };
    this.usersService.ChangePassword(changePasswordModel).subscribe(
      response => {
        console.log(response);
        alert('Password was changed successfully!');
        this.cancelChangePswrd();
      }, err => {
        if (err.error === 'User not found') {
          return this.Logout();
        } else {
          console.error('Error', err);
          this.changePassword.passwordError = true;
          this.changePassword.passwordErrorMsg = err.error;
        }
      }
    );
  }

  public cancelChangePswrd() {
    this.changePassword = {
      password: '',
      confirmPassword: '',
      oldPassword: '',
      passwordError: false,
      passwordErrorMsg: '',
      oldPasswordError: false,
      oldPasswordErrorMsg: ''
    };
    this.passwordEdit = false;
    this.oldPasswordIsConfirmed = false;
  }

}
