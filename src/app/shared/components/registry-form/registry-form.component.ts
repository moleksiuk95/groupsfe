import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthoriseService} from '../../services/authorise.service';

@Component({
  selector: 'app-registry-form',
  templateUrl: './registry-form.component.html',
  styleUrls: ['./registry-form.component.scss']
})
export class RegistryFormComponent implements OnInit {

  @Input()
  public isAdminEdit: boolean;
  @Output()
  public createdUser = new EventEmitter();

  constructor(private authoriseService: AuthoriseService, private router: Router) { }

  authorizeForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ])),
    birthday: new FormControl(''),
    gender: new FormControl('1'),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required)
  });

  submitForm = false;

  registry() {
    this.submitForm = true;
    if (this.authorizeForm.invalid) {
      return;
    } else {
      this.authorizeForm.value.gender = +this.authorizeForm.value.gender;
      const bDate = this.authorizeForm.value.birthday;
      const date = bDate.year + '-' + bDate.month + '-' + bDate.day;
      this.authorizeForm.value.birthday = new Date(date);
      this.authoriseService.Registry(this.authorizeForm.value, this.isAdminEdit).subscribe(
        response => {
          this.createdUser.emit();
        }, error => {
          console.log(error);
        }
      );
    }
  }

  ngOnInit() {
  }

}
