import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BaseNavbarComponent } from './components/BaseNavbar/base-navbar/base-navbar.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { RegistryFormComponent } from './components/registry-form/registry-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  exports: [
    ChangePasswordComponent,
    RegistryFormComponent
  ],
    declarations: [BaseNavbarComponent, ChangePasswordComponent, RegistryFormComponent]
})
export class SharedModule { }
