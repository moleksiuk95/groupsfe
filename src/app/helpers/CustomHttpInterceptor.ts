import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {JWTParser} from './JWTParser';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (JWTParser.CheckToken()) {
      const jwt = JWTParser.currentToken;
      const token = 'Bearer ' + jwt;
      request = request.clone({headers: request.headers.set('Authorization', token)});
    }

    return next.handle(request);
  }
}
