import * as jwt_decode from 'jwt-decode';
import {Role} from '../Models/Role';

export class JWTParser {
  constructor() { }

  public static currentToken: string;
  public static tokenJson: any;

  public static SetCurrentToken() {
    JWTParser.currentToken = sessionStorage.getItem('JWT');
    if (JWTParser.currentToken) {
      JWTParser.tokenJson = jwt_decode(JWTParser.currentToken);
    }
  }

  public static ClearToken() {
    sessionStorage.clear();
    this.currentToken = '';
    this.tokenJson = '';
  }

  public static CheckToken(): boolean {
    if (!JWTParser.currentToken) {
      JWTParser.SetCurrentToken();
    }
    if (JWTParser.currentToken) {
      return true;
    } else {
      return false;
    }
  }

  public static  GetUserEmail(): string {
    if (JWTParser.CheckToken()) {
      console.log(JWTParser.tokenJson);
      const email = JWTParser.tokenJson['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];
      return email;
    } else {
      return null;
    }
  }

  public static GetUserRole(): string {
    if (JWTParser.CheckToken()) {
      return JWTParser.tokenJson.rol;
    }
  }

  public static IsAdmin(): boolean {
    const currentRole = JWTParser.GetUserRole();
    return currentRole === Role.Admin;
  }

  public static IsUser(): boolean {
    const currentRole = JWTParser.GetUserRole();
    return currentRole === Role.User;
  }
}
