import { TestBed, async, inject } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

import { IsAdminGuard } from './is-admin.guard';

describe('IsAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [IsAdminGuard]
    });
  });

  it('should ...', inject([IsAdminGuard, Router], (guard: IsAdminGuard, router: Router) => {
    expect(guard).toBeTruthy();
  }));
});
