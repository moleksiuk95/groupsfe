import { TestBed, async, inject } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [AuthGuard]
    });
  });

  it('should ...', inject([AuthGuard, Router], (guard: AuthGuard, router: Router) => {
    expect(guard).toBeTruthy();
  }));
});
