

import { Component, OnInit } from '@angular/core';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {JWTParser} from "../../helpers/JWTParser";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authoriseService: AuthoriseService,  private router: Router) { }

  submitForm = false;
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  ngOnInit() {
  }

  login() {
    this.submitForm = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.authoriseService.Login(this.loginForm.value).subscribe(
        response => {
          console.log('SUCCESS', response);
          if (JWTParser.IsAdmin()) {
            return this.router.navigate(['/admin-profile']);
          } else {
            return this.router.navigate(['/profile']);
          }
        }, err => {
          console.error('ERROR', err);
        });
    }
  }
}
