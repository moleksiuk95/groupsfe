import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthoriseService} from '../../shared/services/authorise.service';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.scss']
})
export class RegistryComponent implements OnInit {

  constructor(private router: Router) { }

  registry() {
    return this.router.navigate(['/profile']);
  }

  ngOnInit() {
  }

}
