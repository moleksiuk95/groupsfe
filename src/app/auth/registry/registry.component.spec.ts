import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

import { RegistryComponent } from './registry.component';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {RegistryFormComponent} from '../../shared/components/registry-form/registry-form.component';

describe('RegistryComponent', () => {
  let component: RegistryComponent;
  let fixture: ComponentFixture<RegistryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule, NgbDatepickerModule, RouterTestingModule],
      declarations: [ RegistryComponent, RegistryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AuthoriseService, Router], (authoriseService: AuthoriseService, router: Router) => {
    expect(component).toBeTruthy();
  }));
});
