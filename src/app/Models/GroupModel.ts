export class GroupModel {
  public id: number;
  public groupName: string;
  public groupDescr: string;
  public userCount: number;
}
