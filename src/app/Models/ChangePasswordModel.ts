export class ChangePasswordModel {
  public userEmail: string;
  public oldPassword: string;
  public newPassword: string;
}
