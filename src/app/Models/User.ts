export class User {
  firstName: string;
  lastName: string;
  email: string;
  userEmail: string;
  password: string;
  confirmPassword: string;
  birthday: Date;
  gender: number;
}
