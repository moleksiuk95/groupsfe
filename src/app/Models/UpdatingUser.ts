export class UpdatingUser {
  public firstName: string;
  public lastName: string;
  public email: string;
  public oldEmail: string;
  public birthday: Date;
  public gender: number;
}
