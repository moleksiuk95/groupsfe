import {Role} from './Role';

export class UsersInfo {
  public firstName: string;
  public lastName: string;
  public email: string;
  public birthday: Date;
  public gender: number;
  public role: Role;
}
