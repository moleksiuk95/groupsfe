export class RegisteredUser {
  public firstName: string;
  public lastName: string;
  public email: string;
  public birthday: Date;
  public gender: number;
  public photo: string;
  public token: JWTModel;
}

class JWTModel {
  public id: string;
  public authToken: string;
  public expiresIn: number;
}
