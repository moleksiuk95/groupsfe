import {UsersInfo} from './UsersInfo';

export class UsersInGroupsModel {
  allUser: Array<UsersInfo>;
  usersInGroup: Array<string>;
}
