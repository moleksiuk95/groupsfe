import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {RegisteredUser} from '../../Models/RegistererdUser';
import {UsersService} from '../../shared/services/users.service';
import {JWTParser} from '../../helpers/JWTParser';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private authoriseService: AuthoriseService, private usersService: UsersService, private router: Router) { }

  public currentUser: RegisteredUser;
  public profileEdit = false;
  public passwordEdit = false;
  public oldPasswordIsConfirmed = false;
  public changePassword = {
    password: '',
    confirmPassword: '',
    oldPassword: '',
    passwordError: false,
    passwordErrorMsg: '',
    oldPasswordError: false,
    oldPasswordErrorMsg: ''
  };

  private currUserTemp: RegisteredUser;

  ngOnInit() {
    this.currentUser = this.authoriseService.currentUser;
    if (!this.currentUser) {
      this.authoriseService.GetRegisteredUser().subscribe(
        response => {
          this.currentUser = response;
        }, err => {
          console.error(err);
          return this.router.navigate(['/login']);
        }
      );
    }
  }

  public startEdit() {
    this.profileEdit = true;
    this.currUserTemp = Object.assign({}, this.currentUser);
  }

  public cancelEdit() {
    this.profileEdit = false;
    this.currentUser = Object.assign({}, this.currUserTemp);
  }

  public confirmEdit() {
    if (!this.currentUser.firstName || !this.currentUser.lastName) {
      return;
    } else {
      this.usersService.UpdateUser(this.currentUser).subscribe(
        response => {
          this.currentUser = response;
          this.authoriseService.currentUser = this.currentUser;
          this.profileEdit = false;
        }, err => {
          console.error(err);
        }
      );
    }
  }

  public Logout() {
    JWTParser.ClearToken();
    return this.router.navigate(['/login']);
  }

  public checkPassword() {
    if (!this.changePassword.oldPassword) {
      this.changePassword.oldPasswordErrorMsg = 'Old password is required';
      this.changePassword.oldPasswordError = true;
      return;
    }
    this.usersService.CheckOldPassword({email: this.currentUser.email, password: this.changePassword.oldPassword})
      .subscribe(
        response => {
          this.oldPasswordIsConfirmed = true;
          this.changePassword.oldPasswordErrorMsg = '';
          this.changePassword.oldPasswordError = false;
        }, err => {
          console.log('Error', err);
          this.oldPasswordIsConfirmed = false;
          if (err.error === 'User not found') {
            return this.Logout();
          } else {
            this.changePassword.oldPasswordErrorMsg = err.error;
            this.changePassword.oldPasswordError = true;
          }
        }
      );
  }

  public changePswrd() {
    if (!this.changePassword.password || !this.changePassword.confirmPassword) {
      this.changePassword.passwordError = true;
      this.changePassword.passwordErrorMsg = 'This both fields are required';
      return;
    }

    if (this.changePassword.password !== this.changePassword.confirmPassword) {
      this.changePassword.passwordError = true;
      this.changePassword.passwordErrorMsg = 'Passwords does not match';
      return;
    }
    const changePasswordModel = {
      userEmail: this.currentUser.email,
      oldPassword: this.changePassword.oldPassword,
      newPassword: this.changePassword.password
    }
    this.usersService.ChangePassword(changePasswordModel).subscribe(
      response => {
        console.log(response);
        alert('Password was changed successfully!')
        this.cancelChangePswrd();
      }, err => {
        if (err.error === 'User not found') {
          return this.Logout();
        } else {
          console.error('Error', err);
          this.changePassword.passwordError = true;
          this.changePassword.passwordErrorMsg = err.error;
        }
      }
    );
  }

  public cancelChangePswrd() {
    this.changePassword = {
      password: '',
      confirmPassword: '',
      oldPassword: '',
      passwordError: false,
      passwordErrorMsg: '',
      oldPasswordError: false,
      oldPasswordErrorMsg: ''
    };
    this.passwordEdit = false;
    this.oldPasswordIsConfirmed = false;
  }
}
