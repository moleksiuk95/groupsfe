import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { UserProfileComponent } from './user-profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';
import {AuthoriseService} from '../../shared/services/authorise.service';
import {UsersService} from '../../shared/services/users.service';
import {ChangePasswordComponent} from '../../shared/components/change-password/change-password.component';

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule, NgbDatepickerModule, RouterTestingModule],
      declarations: [ UserProfileComponent, ChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AuthoriseService, UsersService, Router],
    (authoriseService: AuthoriseService, usersService: UsersService, router: Router) => {
    expect(component).toBeTruthy();
  }));
});
