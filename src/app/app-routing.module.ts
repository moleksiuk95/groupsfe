import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './auth/login/login.component';
import {HomepageComponent} from './core/homepage/homepage.component';
import {RegistryComponent} from './auth/registry/registry.component';
import {UserProfileComponent} from './users/user-profile/user-profile.component';
import {AuthGuard} from './helpers/guards/auth.guard';
import {IsAdminGuard} from './helpers/guards/is-admin.guard';
import {AdminProfileComponent} from './admin/admin-profile/admin-profile.component';


const routes: Routes = [
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin-profile',
    component: AdminProfileComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registry',
    component: RegistryComponent
  },
  {
    path: '',
    component: LoginComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
